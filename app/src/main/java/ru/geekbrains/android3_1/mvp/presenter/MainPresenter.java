package ru.geekbrains.android3_1.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import ru.geekbrains.android3_1.R;
import ru.geekbrains.android3_1.mvp.model.Model;
import ru.geekbrains.android3_1.mvp.view.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    Model model;

    public MainPresenter() {
        this.model = new Model();
    }

    public int calculateButtonValue(int index){
        model.setAt(index, model.getAt(index) + 1);
        return model.getAt(index);
    }

    public void clickButtonOne() {
        getViewState().setButtonOneValue(calculateButtonValue(0));
    }

    public void clickButtxonTwo() {
        getViewState().setButtonTwoValue(calculateButtonValue(1));
    }

    public void clickButtonThree() {
        getViewState().setButtonTreeValue( calculateButtonValue(2));
    }
}
